# zammad-addon-pgp

Adds PGP integration into [Zammad](https://zammad.org) via [Sequoia](https://sequoia-pgp.org).

## Configuration

Once PGP addon has been successfully installed, there are a few steps required to set it up for use. This is also assuming that Zammad has already been correctly configured for sending and receiving email, and that you have command-line access to a system with the [gnupg](https://gnupg.org) client installed for generating and manipulating keys.

### For Thunderbird users

If you generated and manage your key through a current version of Thunderbird (see [here](https://support.mozilla.org/en-US/kb/openpgp-thunderbird-howto-and-faq) for general information on how to generate keys and use PGP in versions of Thunderbird 78 and above), you will need to use the following steps to prepare for the instructions below assuming you already have a key:

1. Go to `Account Settings -> End-To-End Encryption` for the relevant account and click the 'OpenPGP Key Manager' button to access the key manager.
2. Click the relevant key and select `File -> Backup Secret Key(s) to File` and select a place to save the key file.
3. It will ask you to set a passphrase for the key, make sure to note this down for use below.
4. After the key has been saved to a file, go to that file in a terminal and run `gpg --import <key file>`. It will ask you for the passphrase you just set.
5. Go to #2 below and continue with the instructions.

### Generate helpdesk key

In order to receive encrypted email, the Zammad helpdesk must have a PGP key associated with its email address. You can follow the instructions [here](https://docs.github.com/en/authentication/managing-commit-signature-verification/generating-a-new-gpg-key) in order to generate such a key if you do not already have one; when you get to the step where you enter your email address, use the email address associated with your Zammad installation.

1. Generate the key, using the instructions above. Make a note of the corresponding keyid.
2. You'll need to remove the passphrase from the private key before adding it to Zammad. To do this, edit the key: `gpg --edit-key <keyid>`
3. In the resulting prompt, type `passwd`. Enter the passphrasse you set during key generation, and when you are prompted for a new passphrase just leave it blank and hit 'enter' twice and confirm.
4. As an admin user, go to the Zammad settings panel and under `Channels -> PGP Support` and click the `Add Key` button at the top.
5. In your terminal, export the private key with `gpg --export-secret-key --armor <keyid>` and paste the entire resulting text block including the header and footer into the box for the private key. Do the same for the public key box by exporting with `gpg --export --armor <keyid>`. Select the group the key is associated with ('Users' by default).
6. Submit the changes

Now your helpdesk is configured to accept encrypted email!

### Set user keys

In order to send an encrypted reply to a user who has submitted a ticket, they must have a public key configured. Either that user or an admin can go to settings under `Manage -> Users`, select the user account, and paste their public key in the `PGP Public Key` box. The key can be exported by the user by running `gpg --export --armor <keyid>`, where <keyid> is the keyid of the key associated with the email address with which they are sending an email to the helpdesk. Then submit the changes.

The helpdesk can now send encrypted email to that user!

## Help and Support

Join us in our public matrix channel [#cdr-link-dev-support:matrix.org](https://matrix.to/#/#cdr-link-dev-support:matrix.org?via=matrix.org&via=neo.keanu.im).

## License

[![License GNU AGPL v3.0](https://img.shields.io/badge/License-AGPL%203.0-lightgrey.svg)](https://gitlab.com/digiresilience/link/zamamd-addon-sigarillo/blob/master/LICENSE.md)

This is a free software project licensed under the GNU Affero General
Public License v3.0 (GNU AGPLv3) by [The Center for Digital
Resilience](https://digiresilience.org) and [Guardian
Project](https://guardianproject.info).

🤸
