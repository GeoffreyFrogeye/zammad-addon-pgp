require 'ruby_openpgp'

Rails.application.config.before_configuration do
  #FIXME need icon
  icon = File.read("public/assets/images/icons/pgp.svg")
  doc = File.open("public/assets/images/icons.svg") { |f| Nokogiri::XML(f) }
  if !doc.at_css('#icon-pgp')
    doc.at('svg').add_child(icon)
    Rails.logger.debug "PGP support icon added to icon set"
  else
    Rails.logger.debug "PGP support icon already in icon set"
  end
  File.write("public/assets/images/icons.svg", doc.to_xml)
end

# Rails.application.config.after_initialize do
#   Ticket::Article.add_observer Observer::Ticket::Article::CommunicatePgpSupport.instance
# end
