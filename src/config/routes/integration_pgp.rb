# Copyright (C) 2012-2022 Zammad Foundation, https://zammad-foundation.org/

Zammad::Application.routes.draw do
  api_path = Rails.configuration.api_path

  match api_path + '/integration/pgp',                           to: 'integration/pgp#search',                   via: :post
  match api_path + '/integration/pgp/public_key',               to: 'integration/pgp#public_key_add',          via: :post
  match api_path + '/integration/pgp/public_key',               to: 'integration/pgp#public_key_delete',       via: :delete
  match api_path + '/integration/pgp/public_key',               to: 'integration/pgp#public_key_list',         via: :get
  match api_path + '/integration/pgp/private_key',               to: 'integration/pgp#private_key_add',          via: :post
  match api_path + '/integration/pgp/private_key',               to: 'integration/pgp#private_key_delete',       via: :delete
  match api_path + '/integration/pgp/public_key_download/:id',  to: 'integration/pgp#public_key_download',     via: :get
  match api_path + '/integration/pgp/private_key_download/:id',  to: 'integration/pgp#private_key_download',     via: :get
end
